FROM node:12-buster-slim as builder

RUN apt-get update
RUN npm install pm2 -g
WORKDIR /poc-api
RUN pwd
RUN ls -al
COPY . .
RUN ls -al
RUN npm install
RUN npm run build --prod

FROM node:12-buster-slim
RUN apt-get update
RUN npm install pm2 -g
WORKDIR /poc-api
COPY --from=builder /poc-api ./
RUN pm2 dump
EXPOSE 3003
CMD [ "pm2-runtime dist/main.js"]
