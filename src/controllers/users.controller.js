﻿const express = require('express');
const router = express.Router();
const userService = require('../services/user.service');

// routes
router.get('/hello', hello);
router.get('/helloJson', helloJson);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);
router.post('/', register);
router.get('/', getAll);

module.exports = router;


function hello(req, res) {
    console.log('asdasd');
    return res.send('hola');
}

function helloJson(req, res) {
    console.log('helloJson');
    res.json({name: 'milagros'});
}

function register(req, res, next) {
    userService.create(req.body)
        .then((u) => res.json({uuid: u.uuid}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then((u) => res.json({uuid: u.uuid}))
        .catch(err => {
            res.status(404);
            next(err);
        });
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}
