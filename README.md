## PoC con Node - Express - Docker
Este proyecto incluye una PoC de node con express y Docker

### Requisitos

- Tener instalado [Node v14.16.0 o +](https://nodejs.org/es/) 
- Tener instalado [Docker 19.03.13 o +](https://docs.docker.com/engine/install/) 
- Tener instalado [Docker-compose 1.25.0 o +](https://docs.docker.com/compose/install/)


### Levantarlo entorno de desarrollo
- `npm install`
- `npm start`


### Levantarlo con docker
Posicionarse a la misma altura que el docker-compose.yml
- `docker-compose --build -d up` # buildear y levantar un docker compose
- `docker-compose ps` # revisar containers 
- `docker-compose down` # dar de baja containers
