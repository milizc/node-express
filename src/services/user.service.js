module.exports = {
    getAll,
    getById,
    update,
    delete: _delete,
    create
};

const users = require('../db');

function getAll() {
    return new Promise((resolve, reject) => {
        if (users !== undefined) // ???
            resolve(users);
        else
            reject('not users');
    });
}

function create(user) {
    return new Promise((resolve, reject) => {
        if (users !== undefined) // ???
        {
            users.push(user);
            resolve(user);
        } else
            reject('not users');
    });
}

async function update(id, userParam) {
    return new Promise((resolve, reject) => {
        let user = users.find((u) => u.uuid === id);
        if (user) {
            user.name = userParam.name;
            resolve(user);
        }
        reject('Not found');
    });
}

async function getById(id) {
    return new Promise((resolve, reject) => {
        let user = users.find((u) => u.uuid === id);
        console.log('user', user);
        resolve(user);
    });
}


async function _delete(id) {
    await User.findByIdAndRemove(id);
}
